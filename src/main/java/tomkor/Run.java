package tomkor;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Run extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent tab = FXMLLoader.load(getClass().getResource("/view.fxml"));

        stage.setScene(new Scene(tab));
        stage.setTitle("FX Application");
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
